package com.colinchallenge.checkthemovieskot

import com.colinchallenge.checkthemovieskot.data.MovieSFBasic
import java.util.HashMap
import java.util.concurrent.CopyOnWriteArrayList

class Holder {
    init {
        if (Holder.Bin == null)
            Holder.Bin = HashMap()
    }

    companion object {
        var Bin: HashMap<String, Any>? = null

        var movieList: CopyOnWriteArrayList<MovieSFBasic> = CopyOnWriteArrayList()
        operator fun set(key: String, valObj: Any) {
            Bin!![key] = valObj
        }

        @JvmOverloads
        operator fun get(key: String, remove: Boolean = false): Any? {
            var result: Any? = null

            if (Bin!!.containsKey(key)) {
                result = Bin!![key]
            }

            if (remove) {
                Bin!!.remove(key)
            }
            return result
        }

        fun remove(key: String) {
            Bin!!.remove(key)
        }
    }


}
