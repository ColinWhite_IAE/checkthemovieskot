package com.colinchallenge.checkthemovieskot

import android.os.Handler
import android.os.Message
import android.util.Log
import android.content.Context
import android.content.res.Resources

import com.colinchallenge.checkthemovieskot.data.MovieFiller
import com.colinchallenge.checkthemovieskot.data.MovieSFBasic
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.reactivex.Scheduler

import okhttp3.CacheControl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.ResponseBody
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.functions.Consumer
import io.reactivex.plugins.RxJavaPlugins.onSubscribe
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit


//Pulls other information for helpful use Poster image url, plot lines other are available, this is what we are gettings
class RunnerMoviePuller(private val mov: MovieSFBasic, private val idx: Int, var handler: Handler,
                        val apiServices: MovieCallsApi, val ctx: Context) : Runnable {
    private val movType: String
    private var modTitle: String? = ""
    private var episode: String? = ""

    init {
        episode = ""
        modTitle = mov.title as String
        if (mov.title.toString().contains("- Season ")) {
            modTitle = mov.title.toString().substring(0, mov.title.toString().indexOf("- Season ") - 1)

            if (mov.title.toString().indexOf(" ep") != -1)
                episode = mov.title.toString().substring(mov.title.toString().indexOf(" ep"))

            if (episode != null) {
                modTitle += episode!!.replace(" ep", "&episode=").trim { it <= ' ' }
            }

        }
        modTitle = modTitle!!.trim { it <= ' ' }
        this.movType = if (mov.title.toString().contains("- Season ")) "series" else "movie"
    }

    override fun run() {
        try {
            val mClient = OkHttpClient.Builder().build()
            val cc = CacheControl.Builder().build()

            try {
                var movff: MovieFiller? = null

                apiServices.getMovieDetails(ctx.getString(R.string.apikey), modTitle as String).enqueue(object : Callback<MovieFiller> {
                    override fun onResponse(call: Call<MovieFiller>, response: retrofit2.Response<MovieFiller>) {
                        movff = response.body()
                        mov.filler = movff as MovieFiller
                    }

                    override fun onFailure(call: Call<MovieFiller>, t: Throwable) {
                        Log.e("APICALLERROR", t.message)
                    }
                })
            } catch (eg: Exception) {
                Log.e("RetroCallError", eg.message)
            }

            val msg = Message()
            msg.arg1 = idx
            msg.obj = mov
            handler.sendMessage(msg)
        } catch (ee: Exception) {
            Log.e("FillerPull", ee.message)
        }
    }
}