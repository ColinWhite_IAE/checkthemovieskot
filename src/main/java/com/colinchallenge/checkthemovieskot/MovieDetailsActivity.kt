package com.colinchallenge.checkthemovieskot

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.os.Bundle

import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View

import com.colinchallenge.checkthemovieskot.data.MovieSFBasic
import com.colinchallenge.checkthemovieskot.databinding.MovieDetailCoorBinding

class MovieDetailsActivity : AppCompatActivity() {
    internal var movieItem: MovieSFBasic = MovieSFBasic()
    internal var binding: MovieDetailCoorBinding? = null
    internal var dr: BitmapDrawable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.movie_detail_coor) as MovieDetailCoorBinding
        dr = Holder["backDraw", false] as BitmapDrawable?
        movieItem = Holder.get("movieItem", false) as MovieSFBasic
        binding!!.movieItem = movieItem

        if (movieItem.title != null &&
                movieItem.title != null && supportActionBar != null) {
            supportActionBar!!.setTitle(movieItem.title)
        }

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        if (dr != null) {
            val bitmap = dr!!.bitmap
            binding!!.poster.setImageBitmap(bitmap)
            dr = BitmapDrawable(resources, BlurImage.blur(this, bitmap))

            val color = Color.parseColor("#5d5d5d")
            val cr = ColorDrawable()
            cr.color = color
            supportActionBar!!.setBackgroundDrawable(cr)

            binding!!.appBar.background = dr

            val coll_color = dr!!.bitmap.getPixel(0, 0)
            val hsv = FloatArray(3)
            Color.colorToHSV(coll_color, hsv)
            hsv[2] *= 0.8f
            val finalcolor = Color.HSVToColor(hsv)
            binding!!.colorBlock.setBackgroundColor(finalcolor)

        }
    }

    override fun onPostResume() {
        super.onPostResume()

        if (movieItem.allLocations.size > 0) {
            binding!!.locationFB.setOnClickListener(localFBOnClick(this))
        } else {
            binding!!.locationFB.visibility = View.INVISIBLE;
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
            else -> finish()
        }
        return true
    }


    private inner class localFBOnClick(internal var ctx: Context) : View.OnClickListener {
        override fun onClick(v: View) {
            val intent = Intent(ctx, MovieLocateAct::class.java)
            startActivity(intent)
        }
    }
}
