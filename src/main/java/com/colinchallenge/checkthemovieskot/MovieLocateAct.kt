package com.colinchallenge.checkthemovieskot

import android.content.Intent
import android.location.Geocoder
import android.location.Address
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View


import com.colinchallenge.checkthemovieskot.data.MovieSFBasic
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions

import java.io.IOException


class MovieLocateAct : AppCompatActivity(), OnMapReadyCallback {

    private var mapView: MapView? = null
    private var gmap: GoogleMap? = null
    private var mov: MovieSFBasic? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_locate)
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        mov = Holder["movieItem", false] as MovieSFBasic?
        supportActionBar!!.setTitle(mov!!.title)
        toolbar.title = mov!!.title
        toolbar.subtitle = mov!!.allLocations.size.toString() +
                " location" + (if (mov!!.allLocations.size > 1) "s." else ".") +
                "Touch markers = Google Maps"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        var mapViewBundle: Bundle? = null
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_KEY)
        }

        mapView = findViewById(R.id.map_view)
        mapView!!.onCreate(mapViewBundle)
        mapView!!.getMapAsync(this)
    }


    public override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        var mapViewBundle = outState.getBundle(MAP_VIEW_KEY)
        if (mapViewBundle == null) {
            mapViewBundle = Bundle()
            outState.putBundle(MAP_VIEW_KEY, mapViewBundle)
        }

        mapView!!.onSaveInstanceState(mapViewBundle)
    }

    override fun onResume() {
        super.onResume()
        mapView!!.onResume()

        Snackbar.make(findViewById<View>(R.id.maincoor), "Loading locations.Click markers to open Google Maps", Snackbar.LENGTH_SHORT).show()
        val handler = Handler()
        handler.postDelayed({ GecodeLocater() }, 1500)

    }

    override fun onStart() {
        super.onStart()
        mapView!!.onStart()
    }

    override fun onStop() {
        super.onStop()
        mapView!!.onStop()
    }

    override fun onPause() {
        mapView!!.onPause()
        super.onPause()
    }

    override fun onDestroy() {
        mapView!!.onDestroy()
        super.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView!!.onLowMemory()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        gmap = googleMap

        val SF = LatLng(37.7821582, -122.40658319999999)

        gmap!!.setOnMarkerClickListener(markerClick())
        mapView!!.onResume()

        val cameraPosition = CameraPosition.Builder()
                .target(SF)
                .zoom(11f)
                .bearing(0f)
                .tilt(45f)
                .build()
        gmap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }


    private inner class markerClick : GoogleMap.OnMarkerClickListener {
        override fun onMarkerClick(marker: Marker): Boolean {
            val ll = marker.position
            val myLatitude = ll.latitude
            val myLongitude = ll.longitude

            val intent = Intent(Intent.ACTION_VIEW,
                    Uri.parse("geo:<" + myLatitude + ">,<" + myLongitude + ">?q=<" + myLatitude +
                            ">,<" + myLongitude + ">(" + marker.title + ")"))
            startActivity(intent)

            return false
        }
    }

    fun GecodeLocater() {
        val geocoder = Geocoder(this)
        var addresses: List<Address>? = null

        if (mov!!.allLocations.size > 0) {
            // Toast.makeText(this,"Locations are loading. click Markers to open Google Maps", Toast.LENGTH_LONG).show();
            for (local in mov!!.allLocations) {
                try {
                    // Find a maximum of 3 locations with the name Kyoto
                    addresses = geocoder.getFromLocationName(local, 3)
                } catch (e: IOException) {
                    e.printStackTrace()
                }

                if (addresses != null) {
                    for (loc in addresses) {
                        val opts = MarkerOptions()
                                .position(LatLng(loc.latitude, loc.longitude))
                                .title(local + ":" + loc.getAddressLine(0))

                        gmap!!.addMarker(opts)
                    }
                }
            }
        }
    }

    companion object {
        private val MAP_VIEW_KEY = "MapViewKey"
    }


}
