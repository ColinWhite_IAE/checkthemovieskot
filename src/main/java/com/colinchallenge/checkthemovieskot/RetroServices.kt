package com.colinchallenge.checkthemovieskot

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetroServices(val baseUrl: String) {

    /* making is generic allows for more than one API to be used. */
    fun startServices(): MovieCallsApi {

        val gson: Gson = GsonBuilder().create()
        val mainServices = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()

        return mainServices.create(MovieCallsApi::class.java)
    }


}