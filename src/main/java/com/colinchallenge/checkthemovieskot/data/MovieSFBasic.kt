package com.colinchallenge.checkthemovieskot.data


import android.databinding.BaseObservable
import java.util.ArrayList


class MovieSFBasic  {
    var title: String? = null
    var production_company: String? = null
    var release_year: String? = null
    var writer: String? = null //split by the comma
    var director: String? = null
    var actor_1: String? = null
    var actor_2: String? = null
    var actor_3: String? = null
    var distributor: String? = null
    var fun_facts: String? = null

    //comes from separate source
    var locations: String? = null
    var allLocations = ArrayList<String>()
    var filler = MovieFiller()

    fun addLocation(locStr: String) {
        allLocations.add(locStr)
    }

    fun showSummaryAndLocations(): String {
        val result = StringBuilder()

        if (filler.Plot != null) {
            result.append(filler.Plot!! + "\r\n")
        } else {
            result.append("\r\n")
        }
        if (allLocations.size > 0) {
            result.append("\r\n\r\n( " + allLocations.size.toString() + " Locations Available)")
        } else {
            result.append("\r\n\r\n(NO LOCATIONS)")
        }

        return result.toString()
        return "No Locations"

    }

    fun breakOutLocations(): String {

        val sb = StringBuilder()

        if (allLocations.size > 0) {
            for (s in allLocations) {
                sb.append("- $s")
                sb.append("\r\n")
            }

            return sb.toString()
        } else {
            return "NO LOCATIONS AVAILABLE"
        }

    }

    fun memberString(str: String?, sb: StringBuilder, title: String?): StringBuilder {

        if (!title.isNullOrBlank() && !str.isNullOrBlank())
            sb.append("$title:\r\n")

        if (!str.isNullOrEmpty()) {
            sb.append("       $str\r\n")
        }
        return sb
    }

    fun summaryAndFacts(): String {
        var sb = StringBuilder()
        sb = memberString(filler.Plot, sb, null)
        sb = memberString(fun_facts, sb, "\r\nFUN FACTS")


        return sb.toString().trim { it <= ' ' }
    }

    fun breakOutCast(): String {

        var sb = StringBuilder()

        sb = memberString(director, sb, "Director")
        sb = memberString(production_company, sb, "Production Company")

        if (actor_1 != null || actor_2 != null || actor_3 != null) {
            sb.append("Actors:\r\n")
            sb = memberString(actor_1, sb, null)
            sb = memberString(actor_2, sb, null)
            sb = memberString(actor_3, sb, null)
        }

        sb = memberString(writer, sb, "Writers")
        sb = memberString(distributor, sb, "Distributor")

        return sb.toString()
    }

}
