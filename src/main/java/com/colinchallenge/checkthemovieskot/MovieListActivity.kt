package com.colinchallenge.checkthemovieskot


import android.content.Context

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.design.widget.Snackbar

import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.Filter
import com.colinchallenge.checkthemovieskot.data.MovieSFBasic
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken

import java.util.Comparator
import java.util.concurrent.CopyOnWriteArrayList

import okhttp3.CacheControl
import okhttp3.OkHttpClient
import okhttp3.Request

import kotlinx.android.synthetic.main.movie_main_activity.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import com.colinchallenge.checkthemovieskot.Holder

class MovieListActivity : AppCompatActivity(), Filter.FilterListener {

    lateinit var mainMe: MovieListActivity
    lateinit var movA: MovieAdapter
    lateinit var itemListener: MovieOnItemListener
    //var movieList: CopyOnWriteArrayList<MovieSFBasic>? = CopyOnWriteArrayList()

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.movie_main_activity)

        searchBtn.setOnClickListener(localFBOnClick())
        searchBar.addTextChangedListener(searchTextWatcher())
        mainMe = this

        if(Holder.movieList.isNullOrEmpty()) {
            loadbar.visibility = View.VISIBLE
            searchBar.visibility = View.GONE
            searchBtn.visibility = View.GONE
            cardviewlist.visibility = View.GONE
            Holder.movieList = CopyOnWriteArrayList()

            retroGetPosts()
        }
    }

    //Non RXJava
    fun retroGetPosts()
    {

        val retSrvs = RetroServices(resources.getString(R.string.postURL))

        try{
            retSrvs.startServices().getMainPosts().enqueue(object : Callback<List<MovieSFBasic>> {

            override fun onResponse(call: Call<List<MovieSFBasic>>, response: retrofit2.Response<List<MovieSFBasic>>) {
                updatePosts(response.body())
                loadbar.visibility = View.INVISIBLE
                searchBar.visibility = View.VISIBLE
                searchBtn.visibility = View.VISIBLE
                cardviewlist.visibility = View.VISIBLE
            }

            override fun onFailure(call: Call<List<MovieSFBasic>>, t: Throwable) {
                Log.w("APICALLERROR", t.message)
            }
        })
    } catch (eg: Exception) {
        Log.e("retroGetPostsError", eg.message)
    }


}

    override fun onPostResume() {
        super.onPostResume()

        if (Holder.Bin != null) {
            Holder.remove("backDraw")
            Holder.remove("movieItem")
        }
    }

    private val av = AdapterView.OnItemLongClickListener { arg0, arg1, pos, id ->
        val mov = movA.temporaryData!![pos]

        if (mov.allLocations.size > 0) {
            val intent = Intent(this, MovieLocateAct::class.java)
            Holder["movieItem"] = mov

            this.startActivity(intent)
        } else {
            Snackbar.make(findViewById(R.id.mainRoot), "No locations available", Snackbar.LENGTH_LONG).show()

        }
        true
    }

//    private val handler = object : Handler() {
//        override fun handleMessage(msg: Message) {
//            movieList!![msg.arg1] = msg.obj as MovieSFBasic
//        }
//    }


    private fun updatePosts(posts: List<MovieSFBasic>?) {
        try {
            if (posts!!.size > 0) {
                Holder.movieList = CopyOnWriteArrayList()

                for (xx in posts) {

                    var needAdd = true
                    if (Holder.movieList!!.size > 0) {
                        var idx = 0

                        for (movX: MovieSFBasic in Holder.movieList!!) {
                            if (movX.title == xx.title) {
                                if (!xx.locations.isNullOrEmpty())
                                    movX.addLocation(xx.locations!!)

                                Holder.movieList!![idx] = movX
                                needAdd = false
                                break
                            }
                            idx++
                        }
                    }

                    if (needAdd) {
                        if (!xx.locations.isNullOrBlank())
                            xx.addLocation(xx.locations!!)

                        Holder.movieList!!.add(xx)
                        //movieList!!.forEach(printMovieTitle);
                    }
                }

                Holder.movieList!!.sortWith(Comparator { o1, o2 -> o1.title!!.compareTo(o2.title as String) })

                val rs = RetroServices(applicationContext.getString(R.string.base_debug_url))
                val apiServices: MovieCallsApi = rs.startServices()

                //set filler data
                for (yy in Holder.movieList!!.indices) {
                    val th = Thread(RunnerMoviePuller(Holder.movieList!![yy], yy, movieListHandler, apiServices, applicationContext))
                    th.start()
                }

            }
        }catch(ee:java.lang.Exception){
            Log.e("UPDATEERROR", ee.message)
        }


        cardviewlist.adapter = MovieAdapter(Holder.movieList, mainMe)
        movA = cardviewlist.adapter as MovieAdapter
        cardviewlist.onItemClickListener = MovieOnItemListener(Holder.movieList!!, mainMe)
        itemListener = cardviewlist.onItemClickListener as MovieOnItemListener
        cardviewlist.onItemLongClickListener = av
    }

    private inner class localFBOnClick : View.OnClickListener {

        override fun onClick(v: View) = if (searchBar.getText().length > 0) {
            movA.filter.filter(searchBar.text, mainMe)
        } else {
            resetList()
        }
    }

    override fun onFilterComplete(count: Int) {
        movA.notifyDataSetChanged()
        itemListener!!.movieList = movA.temporaryData as CopyOnWriteArrayList<MovieSFBasic>
        closeKeyboard()
    }

    fun resetList() {
        movA.reset()
        itemListener!!.movieList = movA.temporaryData as CopyOnWriteArrayList<MovieSFBasic>
        movA.notifyDataSetChanged()
    }

    inner class searchTextWatcher : TextWatcher {

        override fun onTextChanged(cs: CharSequence?, arg1: Int, arg2: Int, arg3: Int) {
            if (cs == null || cs.length == 0) {
                resetList()
            }
        }

        override fun beforeTextChanged(arg0: CharSequence, arg1: Int, arg2: Int, arg3: Int) {}

        override fun afterTextChanged(arg0: Editable) {}
    }

    private fun closeKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    //Most internet client calls have to be doen OFF the main UI thread.
    //refresh posts allows for those changes.
    private inner class refreshPosts : AsyncTask<Void, Void, String>() {

        private var movieArray: List<MovieSFBasic>? = null

        internal var mClient = OkHttpClient.Builder().build()
        private val cc = CacheControl.Builder().build()

        override fun onPreExecute() {
            super.onPreExecute()
            loadbar.visibility = View.VISIBLE
            searchBar.visibility = View.GONE
            searchBtn.visibility = View.GONE
        }

        override fun doInBackground(vararg vd: Void): String? {
            try {

                val request = Request.Builder()
                        .url("https://data.sfgov.org/resource/wwmu-gmzc.json")
                        .cacheControl(cc)
                        .build()

                val response = mClient.newCall(request).execute()

                if (response.body() != null) {
                    val rb = response.body()
                    val result = rb!!.string()

                    val gson: Gson = GsonBuilder().create()
                    //val listType = object : TypeToken<List<String>>() { }.type
                    val collectionType = object : TypeToken<CopyOnWriteArrayList<MovieSFBasic>?>() {}.type
                    movieArray = gson.fromJson<CopyOnWriteArrayList<MovieSFBasic>?>(result, collectionType)
                    //val strInt: String = movieArray!!.size.toString()
                    // Log.i("numbers", strInt)

                }
            } catch (ee: Exception) {
                Log.e("refreshPosts", ee.message)
            }

            return ""
        }

        override fun onPostExecute(result: String) {
            updatePosts(movieArray)
            loadbar.visibility = View.INVISIBLE
            searchBar.visibility = View.VISIBLE
            searchBtn.visibility = View.VISIBLE
            cardviewlist.visibility = View.VISIBLE
        }
    }

    companion object movieListHandler: Handler() {
        override fun handleMessage(msg: Message) {
            Holder.movieList!![msg.arg1] = msg.obj as MovieSFBasic
        }
    }
}

