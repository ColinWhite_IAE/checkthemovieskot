package com.colinchallenge.checkthemovieskot

import com.colinchallenge.checkthemovieskot.data.MovieFiller
import com.colinchallenge.checkthemovieskot.data.MovieSFBasic

import retrofit2.Call
import retrofit2.http.GET

import retrofit2.http.Query

interface MovieCallsApi {
    @GET("/")
    fun getMovieDetails(
            @Query("apikey") apikey: String,
            @Query(value = "t", encoded = true) mdTitle: String): Call<MovieFiller>

    @GET("resource/wwmu-gmzc.json")
    fun getMainPosts():Call<List<MovieSFBasic>>

}

