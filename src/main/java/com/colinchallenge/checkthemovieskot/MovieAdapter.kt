package com.colinchallenge.checkthemovieskot

import android.content.Context
import android.databinding.DataBindingUtil
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView

import com.colinchallenge.checkthemovieskot.data.MovieSFBasic
import com.colinchallenge.checkthemovieskot.databinding.MovieListCardBinding
import com.squareup.picasso.Picasso

import java.util.concurrent.CopyOnWriteArrayList

class MovieAdapter(var temporaryData: CopyOnWriteArrayList<MovieSFBasic>?, internal var context: Context) : BaseAdapter(), Filterable {
    internal var movieList: CopyOnWriteArrayList<MovieSFBasic>?
    var picassoSingle: Picasso
    internal var holder = Holder()
    private val v: ViewHolder? = null

    init {
        if (temporaryData == null) {
            throw IllegalArgumentException("postData must not be null")
        }
        movieList = temporaryData!!
        picassoSingle = Picasso.with(context)
    }

    fun reset() {
        temporaryData = movieList
        notifyDataSetChanged()
    }

    override fun getItem(position: Int): MovieSFBasic {
        return temporaryData!!.get(position)
    }

    override fun getView(position: Int, listRow: View?, parent: ViewGroup): View {
        var listRowView = listRow
        val binding: MovieListCardBinding?
        val movieItm: MovieSFBasic?

        movieItm = getItem(position)

        if (listRowView == null) {
            listRowView = LayoutInflater.from(context).inflate(R.layout.movie_list_card, null)
            binding = DataBindingUtil.bind(listRowView!!)
            listRowView.tag = binding

        } else {
            binding = listRowView.tag as MovieListCardBinding
        }


        try {
            binding!!.movieItem = movieItm
            val vh = ViewHolder()
            vh.tv1 = binding.title
            vh.tv2 = binding.releaseYear
            vh.tv3 = binding.summary
            vh.iv = binding.moviePoster
            vh.urlStr = movieItm.director
            vh.rl = binding.mainCardBack
            vh.position = position

        } catch (ee: NullPointerException) {
            Log.w("BindIssue", ee.message)
            throw ee

        }

        val postImage = binding.moviePoster


        val imgUrl = movieItm.filler.Poster
        //

        //        //Either load the feature image or load the icon for filler
        if (imgUrl != null && imgUrl.length > 0) {
            picassoSingle.load(imgUrl).resize(800, 1000).centerInside().into(postImage, picassoCallBack(postImage, binding.mainCardBack, listRowView))

        } else {
            postImage.setImageDrawable(context.getDrawable(R.drawable.no_poster))
        }

        return binding.root
    }

    override fun getItemId(position: Int): Long {
        return -1L
    }

    override fun getCount(): Int {
        return temporaryData!!.size
    }

    override fun getFilter(): Filter {

        return FilterWiget()
    }

    inner class FilterWiget : Filter() {

        @SuppressWarnings("unchecked")
        override fun publishResults(constraint: CharSequence, results: Filter.FilterResults) {
            temporaryData = results.values as CopyOnWriteArrayList<MovieSFBasic>?
            notifyDataSetChanged()
        }

        @SuppressWarnings("unchecked")
        override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {

            val results = Filter.FilterResults()
            val filteredList = CopyOnWriteArrayList<MovieSFBasic>()
            if (constraint == null || constraint.isEmpty()) {
                results.values = movieList
                results.count = movieList!!.size
            } else {
                for (i in temporaryData!!.indices) {
                    val data = temporaryData!!.get(i).title
                    if (data!!.toLowerCase().contains(constraint.toString().toLowerCase())) {
                        filteredList.add(temporaryData!!.get(i))
                    }
                }
                results.values = filteredList
                results.count = filteredList.size
            }
            return results
        }
    }


    private inner class picassoCallBack(var postImage: ImageView, var vw: View, internal var cardBack: View) : com.squareup.picasso.Callback {

        override fun onSuccess() {
            vw.setBackgroundColor(Color.BLACK)

        }

        override fun onError() {
            postImage.setImageDrawable(context.getDrawable(R.drawable.no_poster))
        }

    }

    private inner class ViewHolder {
        internal var tv1: TextView? = null
        internal var tv2: TextView? = null
        internal var tv3: TextView? = null
        internal var iv: ImageView? = null
        internal var rl: RelativeLayout? = null
        internal var position: Int = 0
        var urlStr: String? = null

    }

}
