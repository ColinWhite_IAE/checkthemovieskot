package com.colinchallenge.checkthemovieskot

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.AdapterView
import android.widget.ImageView

import com.colinchallenge.checkthemovieskot.data.MovieSFBasic

import java.util.ArrayList
import java.util.concurrent.CopyOnWriteArrayList

class MovieOnItemListener(var movieList: CopyOnWriteArrayList<MovieSFBasic>?, internal var context: Context) : AdapterView.OnItemClickListener {

    override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        val mov = movieList!![position]
        val posterImage = view.findViewById<ImageView>(R.id.moviePoster)
        val dr = posterImage.drawable

        val detailsIntent = Intent(context, MovieDetailsActivity::class.java)
        Holder["movieItem"] = mov
        Holder["backDraw"] = dr

        context.startActivity(detailsIntent)

    }
}
